package com.iklick.shiftscheduler.data.repository

import com.iklick.shiftscheduler.application.services.ApiService
import com.iklick.shiftscheduler.data.entities.EngineerListResponse
import com.iklick.shiftscheduler.utils.UseCaseResult

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
interface EngineerListRepository {
    suspend fun getEmployeeList(): UseCaseResult<EngineerListResponse>
}

class EngineerListRepositoryImpl(val apiService: ApiService) : EngineerListRepository {
    override suspend fun getEmployeeList(): UseCaseResult<EngineerListResponse> {
        return try {
            val result = apiService.getEngineersAsync().await()
            UseCaseResult.Success(result)
        } catch (ex: Exception) {
            UseCaseResult.Error(ex)
        }
    }

}