package com.iklick.shiftscheduler.data.repository

import com.iklick.shiftscheduler.application.services.scheduler.ScheduleService
import com.iklick.shiftscheduler.data.entities.Engineer
import com.iklick.shiftscheduler.data.entities.Schedule

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
interface ScheduleRepository {
    fun getSchedule(engineerList: ArrayList<Engineer>): List<Schedule>
}


class ScheduleRepositoryImpl : ScheduleRepository {
    override fun getSchedule(engineerList: ArrayList<Engineer>): List<Schedule> {
        val scheduleService = ScheduleService(engineerList)
        return scheduleService.schedules!!
    }

}