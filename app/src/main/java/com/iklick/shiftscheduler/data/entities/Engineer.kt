package com.iklick.shiftscheduler.data.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
@Parcelize
data class Engineer(
    var id: Int?,
    var name: String?
) : Parcelable