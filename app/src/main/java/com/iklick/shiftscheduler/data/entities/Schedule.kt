package com.iklick.shiftscheduler.data.entities

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
data class Schedule(val scheduleId: String, val day: Int, val shiftEngineers: List<Engineer>)