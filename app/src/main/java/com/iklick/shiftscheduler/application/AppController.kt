package com.iklick.shiftscheduler.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.iklick.shiftscheduler.modules.appModules
import com.iklick.shiftscheduler.modules.engineerListModule
import com.iklick.shiftscheduler.modules.scheduleModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
class AppController : Application() {


    override fun onCreate() {
        super.onCreate()
        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        startKoin {
            androidContext(this@AppController)
            modules(appModules, engineerListModule, scheduleModule)

        }

    }


    companion object {
        var instance: AppController? = null
    }
}