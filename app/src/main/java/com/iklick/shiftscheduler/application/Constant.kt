package com.iklick.shiftscheduler.application

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
object Constant {

    val CUSTOM_INVALID_INT = -1

    const val BASE_URL = "http://private-3bfddf-supportwheeloffate2.apiary-mock.com/"

    // Number of working days in schedule period
    const val SCHEDULE_PERIOD_DAYS = 10

    // Engineers required max shifts per day
    const val MAX_SHIFTS_PER_DAY = 2

    // Engineers Should have a day / days off (consecutive)
    const val ENGINEER_OFF_DAYS = 1

    // Engineer shifts
    const val MAX_SHIFTS_PER_ENGINEER = 2

    const val ENGINEER_LIST = "engineer_list"


}