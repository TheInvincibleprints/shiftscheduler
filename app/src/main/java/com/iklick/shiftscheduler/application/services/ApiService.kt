package com.iklick.shiftscheduler.application.services

import com.iklick.shiftscheduler.data.entities.EngineerListResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
interface ApiService {

    @GET("engineers")
    fun getEngineersAsync(): Deferred<EngineerListResponse>
}