package com.iklick.shiftscheduler.ui.fragments.engineer

import androidx.lifecycle.MutableLiveData
import com.iklick.shiftscheduler.base.BaseViewModel
import com.iklick.shiftscheduler.data.entities.Engineer
import com.iklick.shiftscheduler.data.repository.EngineerListRepository
import com.iklick.shiftscheduler.utils.UseCaseResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
class EngineerListViewModel(private val employeeListRepository: EngineerListRepository) :
    BaseViewModel() {
    val showError = MutableLiveData<String>()
    val showLoading = MutableLiveData<Boolean>()
    val engineerList = MutableLiveData<ArrayList<Engineer>>()


    fun getEngineers() {
        showLoading.value = true
        launch {
            val result = withContext(Dispatchers.IO) { employeeListRepository.getEmployeeList() }
            showLoading.value = false
            when (result) {
                is UseCaseResult.Success -> engineerList.value = ArrayList(result.data.engineers)
                is UseCaseResult.Error -> showError.value = "Something went wrong"
            }
        }
    }
}
