package com.iklick.shiftscheduler.ui.fragments.engineer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.iklick.shiftscheduler.R
import com.iklick.shiftscheduler.application.Constant
import com.iklick.shiftscheduler.ui.activity.MainActivity
import com.iklick.shiftscheduler.ui.fragments.schedule.ScheduleFragment
import io.reactivex.internal.functions.ObjectHelper
import io.reactivex.internal.functions.ObjectHelper.requireNonNull
import kotlinx.android.synthetic.main.employee_list_fragment.view.*
import kotlinx.android.synthetic.main.view_toolbar.*
import org.koin.android.ext.android.inject

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
class EngineerListFragment : Fragment() {

    private lateinit var toolbar: Toolbar
    private lateinit var viewParent: View
    private lateinit var adapter: EngineerAdapter
    private val viewModel: EngineerListViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewParent = inflater.inflate(R.layout.employee_list_fragment, container, false)
        return viewParent
    }

    override fun onStart() {
        super.onStart()
        initToolbar()
        initView()
        initViewModel()
    }

    private fun initToolbar() {

        val main = activity as MainActivity

        toolbar = main.findViewById(R.id.view_toolbar)

        main.setSupportActionBar(toolbar)
        main.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        main.supportActionBar?.setDisplayShowHomeEnabled(false)
        main.supportActionBar?.title = resources.getString(R.string.engineers)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getEngineers()
    }

    private fun initView() {
        viewParent.listEngineer.layoutManager =
            LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        adapter = EngineerAdapter()
        viewParent.listEngineer.adapter = adapter

        viewParent.buttonCreateSchedule.setOnClickListener {
            val args = Bundle()
            args.putParcelableArrayList(Constant.ENGINEER_LIST, viewModel.engineerList.value!!)
            findNavController().navigate(R.id.action_employeeListFragment_to_scheduleFragment, args)

//            val ldf = ScheduleFragment()
//            ldf.arguments = args
//            fragmentManager!!.beginTransaction().replace(R.id.frg_main_container, ldf).commit()
//            (requireNonNull(
//                activity,
//                "context cannot be null"
//            ) as MainActivity).proceedItem(ScheduleFragment.FRAGMENT_TAG, args)

        }
    }

    private fun initViewModel() {
        viewModel.showLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                (activity as MainActivity).showProgress()
            } else {
                (activity as MainActivity).hideProgress()
            }
        })

        viewModel.showError.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.engineerList.observe(viewLifecycleOwner, Observer {
            if (it.isEmpty()) {
                viewParent.buttonCreateSchedule.visibility = View.GONE
            } else {
                viewParent.buttonCreateSchedule.visibility = View.VISIBLE
            }
            adapter.setData(it)
        })
    }

    companion object {
        val FRAGMENT_TAG = "EngineersFragment"

        fun newInstance(): EngineerListFragment {
            return EngineerListFragment()
        }

    }
}
