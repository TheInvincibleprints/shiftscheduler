package com.iklick.shiftscheduler.ui.fragments.schedule

import androidx.lifecycle.MutableLiveData
import com.iklick.shiftscheduler.base.BaseViewModel
import com.iklick.shiftscheduler.data.entities.Engineer
import com.iklick.shiftscheduler.data.entities.Schedule
import com.iklick.shiftscheduler.data.repository.ScheduleRepository
import kotlinx.coroutines.launch

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
class ScheduleViewModel(var scheduleRepository: ScheduleRepository) : BaseViewModel() {
    var scheduleList = MutableLiveData<List<Schedule>>()


    fun getSchedule(engineerList: ArrayList<Engineer>) {
        launch {
            scheduleList.value = scheduleRepository.getSchedule(engineerList)
        }
    }
}
