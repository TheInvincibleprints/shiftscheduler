package com.iklick.shiftscheduler.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import com.iklick.shiftscheduler.R
import com.iklick.shiftscheduler.application.Constant.CUSTOM_INVALID_INT
import com.iklick.shiftscheduler.base.BaseActivity
import com.iklick.shiftscheduler.ui.fragments.engineer.EngineerListFragment
import com.iklick.shiftscheduler.ui.fragments.schedule.ScheduleFragment
import io.reactivex.internal.functions.ObjectHelper
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mainViewHolder: MainViewHolder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        if (savedInstanceState == null) {
//            mainViewHolder = MainViewHolder(this)
//
//            setSupportActionBar(mainViewHolder!!.toolbar)
//
//            // proceed to engineers fragment.
//            switchItem(EngineerListFragment.FRAGMENT_TAG, savedInstanceState)
//
//
//        }
    }

    fun showProgress() {
        progressMain.visibility = View.VISIBLE
    }

    fun hideProgress() {
        progressMain.visibility = View.GONE
    }

}
