package com.iklick.shiftscheduler.modules

import com.iklick.shiftscheduler.data.repository.ScheduleRepository
import com.iklick.shiftscheduler.data.repository.ScheduleRepositoryImpl
import com.iklick.shiftscheduler.ui.fragments.schedule.ScheduleViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
val scheduleModule = module {
    factory<ScheduleRepository> { ScheduleRepositoryImpl() }
    // Specific viewModel pattern to tell Koin how to build MainViewModel
    viewModel { ScheduleViewModel(scheduleRepository = get()) }
}