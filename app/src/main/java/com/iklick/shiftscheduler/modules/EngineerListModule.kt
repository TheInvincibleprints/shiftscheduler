package com.iklick.shiftscheduler.modules

import com.iklick.shiftscheduler.data.repository.EngineerListRepository
import com.iklick.shiftscheduler.data.repository.EngineerListRepositoryImpl
import com.iklick.shiftscheduler.ui.fragments.engineer.EngineerListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 ******************************************************
 * Project : ShiftScheduler
 ******************************************************
 * Created by Emmanuel on 04-Nov-2019.
 */
val engineerListModule = module {
    factory<EngineerListRepository> { EngineerListRepositoryImpl(apiService = get()) }
    // Specific viewModel pattern to tell Koin how to build MainViewModel
    viewModel { EngineerListViewModel(employeeListRepository = get()) }
}